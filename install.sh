export DEBIAN_FRONTEND=noninteractive

# Install all dependencies
apt-get update
apt-get upgrade -y
apt-get install build-essential gcc apache2 unzip nghttp2 libnghttp2-dev libssl-dev git pcre2-utils cmake automake tcl tcl-dev -y

# Install nginx with headers more
git clone https://github.com/angristan/nginx-autoinstall.git
cd nginx-autoinstall
git checkout b92170a19ad0400ed5cfb113754533bddd9ccf5d
chmod +x nginx-autoinstall.sh
HEADLESS=y \
NGINX_VER=STABLE \
HEADERMOD=y \
bash ./nginx-autoinstall.sh
cd ..

# Install libnshttp2
git clone https://fminic@bitbucket.org/fminic/libnshttp2.git
cd libnshttp2/
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ../
make install
cd ../../

# Install naviserver
git clone https://fminic@bitbucket.org/naviserver/naviserver.git
cd naviserver
bash autogen.sh
./configure --prefix=/usr/local/ns
make install
cd ../

# Install naviserver-h2
git clone https://fminic@bitbucket.org/fminic/naviserver-h2.git
cd naviserver-h2
bash autogen.sh
./configure --prefix=/usr/local/ns-h2
make install
cd ../

# Remove existing configurations
rm -rf /etc/apache2/apache2.conf /etc/apache2/sites-enabled /etc/apache2/sites-available
rm -rf /etc/nginx/nginx.conf /etc/nginx/sites-enabled /etc/nginx/sites-available
rm -f /usr/local/ns/conf/nsd-config.tcl
rm -f /usr/local/ns-h2/conf/nsd-config.tcl

# Copy config files
wget https://masterthesiswu.s3.eu-central-1.amazonaws.com/httpload_config.tar.xz
tar xvf httpload_config.tar.xz
cp -r httpload_config/www/* /var/www/
mount -t tmpfs -o rw,size=1G tmpfs /var/www
cp -r httpload_config/www/* /var/www/
cp -r httpload_config/apache2/* /etc/apache2/
cp -r httpload_config/nginx/* /etc/nginx/

cp httpload_config/nsd-config.tcl /usr/local/ns/conf/nsd-config.tcl
cp httpload_config/nsdh2-config.tcl /usr/local/ns-h2/conf/nsd-config.tcl

# Enable Apache2 modules
a2enmod ssl http2 headers

# Copy NaviServer systemd service files
cp httpload_config/nsd.service /etc/systemd/system/
cp httpload_config/nsd-h2.service /etc/systemd/system/

# Generate self-signed certificates
openssl dhparam -out /etc/ssl/private/dhparams.pem 2048
openssl req -new -x509 -sha256 -newkey rsa:2048 -days 365 -nodes -config httpload_config/openssl.cnf -keyout /etc/ssl/private/server.key -out /etc/ssl/private/server.pem
cat /etc/ssl/private/server.pem /etc/ssl/private/server.key /etc/ssl/private/dhparams.pem > /etc/ssl/private/naviserver.pem

# Reload libraries
sudo ldconfig

# Reload and start all services
systemctl daemon-reload
systemctl enable apache2
systemctl enable nginx
systemctl enable nsd
systemctl enable nsd-h2
systemctl restart apache2
systemctl restart nginx
systemctl restart nsd
systemctl restart nsd-h2