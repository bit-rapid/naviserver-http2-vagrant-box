# NaviServer, Apache, Nginx Vagrant HTTP testing box #

Vagrant box which sets up Apache, Nginx, NaviServer with HTTP/1.1 and HTTP/2, some static test files and exposes the services under local ports:

* 6080 - NaviServer HTTP/1.1
* 5080 - NaviServer HTTP/2
* 7070 - Apache HTTP/1.1
* 7071 - Apache HTTP/2
* 8080 - Nginx HTTP/1.1
* 8081 - Nginx HTTP/2
* 7072 - Apache HTTP/1.1 with large headers
* 7073 - Apache HTTP/2 with large headers
* 8082 - Nginx HTTP/1.1 with large headers
* 8083 - Nginx HTTP/2 with large headers

## Prerequisites
In order to use the tool you need to have a Vagrant provider (e.g. https://www.virtualbox.org/) and Vagrant installed.

* You can download the latest Vagrant software under https://www.vagrantup.com/downloads.
* If you want to use Oracle VirtualBox as your provider you can download it from https://www.virtualbox.org/.

## Purpose
This Vagrant box was created as a test setup for HTTP/1.1 and HTTP/2, in particular to compare the [naviserver-h2](https://bitbucket.org/fminic/naviserver-h2) and [nshttp2](https://bitbucket.org/fminic/libnshttp2/src/master/) implementations to Apache and Nginx.

## How do I use the tool? ##

### Vagrant ###
Once you have installed Vagrant and a compatible provider, you need to switch the repository directory and run:
```sh
vagrant up
```

In order to stop the running box:
```sh
vagrant halt
```

If you need to ssh to the vagrant box, run:
```sh
vagrant ssh
```

To stop and destroy the virtual machine:
```sh
vagrant destroy
```

If you want to change the Virtual Machien parameters, you can modify the CPU count and memory in the Vagrantfile.

### Provided static files ###
The following static files are available:

small_small.html - Small number of small files
medium_small.html - Medium number of small files
large_small.html - Large number of small files
medium_small.html - Small number of medium files
medium_medium.html - Medium number of medium files
medium_large.html - Large number of medium files
large_small.html - Small number of large files
large_medium.html - Medium number of large files
large_large.html - Large number of large files

In order to open one of the pages you can use one of the ports above with HTTPS, e.g.:

``https://localhost:5080/small_small.html``

# NOTE
Wait for the Vagrant box to be fully setup before you start using the web servers, this is the case when `vagrant up` exits.

If you halt the vagrant box, you will also close the RAMdisk setup for the static files, so you should expect the performance to drop. You can set it up by connecting to the boxusing `vagrant ssh` and running:
```sh
sudo mount -t tmpfs -o rw,size=1G tmpfs /var/www
cp -r ~/httpload_config/www/* /var/www/
```